var APP = {
    /** Originalna zawartość pliku JSON */
    booksOrigin: {},

    /** Robocza zawartość pliku JSON */
    books: {},

    /**
     * Wykonanie funkcji po załadowaniu struktury dokumentu
     */
    start: function() {
        APP.loadJSON('books.json');
        APP.inputValueValidation();
        APP.searchFormAction();
        APP.setParamsToForm();
        APP.clearFormAction();
        APP.hidePopupAction();
    },

    /**
     * Przywrócenie obiektu otrzymanego po wczytaniu pliku JSON
     */
    restoreData: function() {
        APP.books = APP.booksOrigin;
    },

    /**
     * Załadowanie danych z pliku JSON i utworzenie początkowej struktury
     * @param {string} path - ścieżka do pliku
     */
    loadJSON: function (path) {

        var xhr = new XMLHttpRequest();
        xhr.onreadystatechange = function() {
            if (xhr.readyState === XMLHttpRequest.DONE && 200 === xhr.status) {
                APP.booksOrigin = JSON.parse(xhr.responseText);
                APP.restoreData();
                APP.setPagesLimit();
                APP.handleSortAction();
                APP.setBooks();
                APP.popupShowAction();
            }
        };

        xhr.open("GET", path, true);
        xhr.send();
    },

    /**
     * Załadowanie księżek do kontenera w strukturze dokumentu
     */
    setBooks: function() {
        if(0 === APP.books.length) {
            return;
        }

        var boxContainers = document.getElementsByClassName('js-main__list');

        if(0 === boxContainers.length) {
            return;
        }

        var container = boxContainers[0];
        container.innerHTML = "";

        for(var bookIndex in APP.books) {
            var number = parseInt(bookIndex) + 1;
            var box = APP.createBox(APP.books[bookIndex], number);

            container.appendChild(box);
        }

    },

    /**
     * Pobieranie parametrów z adresu URL
     * @see [StackOverflow]{@link https://stackoverflow.com/questions/979975/how-to-get-the-value-from-the-get-parameters}
     * @returns {Object}
     */
    getUrlParams: function () {
        var query = window.location.search.substring(1)
        var vars = query.split("&");
        var queryString = {};

        for (var i = 0; i < vars.length; i++) {
            var pair = vars[i].split("=");
            if (typeof queryString[pair[0]] === "undefined") {
                queryString[pair[0]] = decodeURIComponent(pair[1]);
            } else if (typeof queryString[pair[0]] === "string") {
                var arr = [queryString[pair[0]], decodeURIComponent(pair[1])];
                queryString[pair[0]] = arr;
            } else {
                queryString[pair[0]].push(decodeURIComponent(pair[1]));
            }
        }

        return queryString;
    },

    /**
     * Kontrola wejścia pola liczby stron
     * @see [StackOverflow]{@link https://stackoverflow.com/questions/469357/html-text-input-allows-only-numeric-input}
     */
    inputValueValidation: function() {
        document.getElementById('pages').addEventListener('keydown', function(e) {
            var key = window.event ? e.keyCode : e.which;
            if (e.keyCode === 8 || e.keyCode === 46 || e.keyCode === 13) {
                return;
            } else if ( key < 48 || key > 57 ) {
                e.preventDefault();
            } else {
                return;
            }
        });
    },

    /**
     * Tworzenie dowolnego elementu HTML
     * @param {string} elementType - rodzaj elementu
     * @param {string} className - wartość atrybutu class
     * @param {object} attributes - atrybuty elementu
     * @param {string|Element} html - zawartość elementu
     * @returns {Element}
     */
    createHtmlElement: function(elementType, className, attributes, html) {
        className = className || '';
        attributes = attributes || '';
        html = html || '';

        var element = document.createElement(elementType);

        if(true === Boolean(className)) {
            element.className = className;
        }

        if(true === Boolean(attributes) && "object" === typeof attributes) {
            var attributeNames = Object.keys(attributes);
            for(var i = 0; i < attributeNames.length; i++) {
                var attributeName = attributeNames[i];
                var attributeValue = attributes[attributeName];
                element.setAttribute(attributeName, attributeValue);
            }
        }

        if(true === Boolean(html)) {
            element.innerHTML = html;
        }

        return element;
    },

    /**
     * Tworzenie poszczególnych elementów dla boksa
     * @param {object} data - Dane niezbędne do wypełnienie boksu
     * @param {number} number - numer porządkowy widoczny w boksie
     * @returns {{box: (*|Element), boxDescription: (*|Element), boxNumber: (*|Element), imageHolder: (*|Element), image: (*|Element), title, author: (*|Element), releaseDate, pages, linkContainer: (*|Element), link}}
     */
    createBoxElements: function(data, number) {
        return {
            box: APP.createHtmlElement(
                'li',
                'box'
            ),
            boxDescription: APP.createHtmlElement(
                'div',
                'box__description'
            ),
            boxNumber: APP.createHtmlElement(
                'div',
                'box__number',
                '',
                number || ''
            ),
            imageHolder: APP.createHtmlElement(
                'a',
                'box__image-holder js-box__image-holder',
                {
                    'href': data.cover.large || '#'
                }
            ),
            image: APP.createHtmlElement(
                'img',
                'box__image',
                {
                    'src': data.cover.small || ''
                }
            ),
            title: APP.createHtmlElement(
                'div',
                'box__description-title',
                '',
                (data.title || 'Brak informacji')
            ),
            author: APP.createHtmlElement(
                'div',
                'box__description-author'
            ),
            releaseDate: APP.createHtmlElement(
                'div',
                'box__description-release',
                '',
                'Release Date: ' + (data.releaseDate || 'Brak informacji')
            ),
            pages: APP.createHtmlElement(
                'div',
                'box__description-pages',
                '',
                'Pages: ' + (data.pages || 'Brak informacji')
            ),
            linkContainer: APP.createHtmlElement(
                'div',
                'box__description-link',
                '',
                'Link: '
            ),
            link: APP.createHtmlElement(
                'a',
                'box__description-link--url',
                {
                    'href': data.link || '#',
                    'target': 'blank'
                },
                'shop'
            )
        };
    },

    /**
     *  Tworzenie boksa dla listy wyników
     * @param {object} data - Dane niezbędne do wypełnienie boksu
     * @param {number} number - numer porządkowy widoczny w boksie
     * @returns {Element}
     */
    createBox: function(data, number) {

        var boxElements = APP.createBoxElements(data, number);

        boxElements.linkContainer.appendChild(boxElements.link);

        boxElements.boxDescription.appendChild(boxElements.title);
        boxElements.boxDescription.appendChild(boxElements.author);
        boxElements.boxDescription.appendChild(boxElements.releaseDate);
        boxElements.boxDescription.appendChild(boxElements.pages);
        boxElements.boxDescription.appendChild(boxElements.linkContainer);

        boxElements.box.appendChild(boxElements.boxNumber);

        boxElements.box.appendChild(boxElements.imageHolder).appendChild(boxElements.image);

        boxElements.box.appendChild(boxElements.boxDescription);

        return boxElements.box;
    },

    /**
     * Filtrowanie wyników na podstawie wprowadzonych danych
     */
    setPagesLimit: function() {
        var urlParams = APP.getUrlParams();

        if("undefined" === typeof urlParams.pages) {
            return;
        }

        APP.books = APP.books.filter(function(element) {
            return element.pages >= urlParams.pages;
        });

    },

    /**
     * Sortowanie wyników po dacie wydania (rosnąco)
     */
    sortByDate: function() {
        APP.books.sort(function(a, b){
            var yearMonthA = a.releaseDate.split('/');
            var yearMonthB = b.releaseDate.split('/');

            if("undefined" === yearMonthA[1] || "undefined" === yearMonthB[1]) {
                return;
            }

            var monthA = yearMonthA[0];
            var yearA = yearMonthA[1];

            var monthB = yearMonthB[0];
            var yearB = yearMonthB[1];

            var dateA = new Date(yearA, monthA).getTime();
            var dateB = new Date(yearB, monthB).getTime();

            if(isNaN(dateA) || isNaN(dateB)) {
                return;
            }

            return dateA > dateB ? 1 : -1;
        });

        APP.setBooks();
    },

    /**
     * Sortowanie wyników po imieniu i nazwisku autora (alfabetycznie, rosnąco)
     */
    sortByFullName: function () {
        APP.books.sort(function (a, b) {
            return a.author.localeCompare(b.author);
        });
    },

    /**
     * Sortowanie wyników po samym nazwisku (alfabetycznie, rosnąco)
     */
    sortBySurname: function () {
        APP.books.sort(function (a, b) {
            var nameSurnameA = a.author.split(' ');
            var nameSurnameB = b.author.split(' ');

            if("undefined" === typeof nameSurnameA[1] || "undefined" === typeof nameSurnameB[1]) {
                return;
            }

            var surnameA = nameSurnameA[1];
            var surnameB = nameSurnameB[1];

            return surnameA.localeCompare(surnameB);
        });
    },

    /**
     * Sortowani wyników po ilości stron (rosnąco)
     */
    sortByPages: function () {
        APP.books.sort(function (a, b) {
            // Inwersja sortowania
            // return parseInt(b.pages) - parseInt(a.pages);
            return parseInt(a.pages) - parseInt(b.pages);
        });
    },

    /**
     * Wykonanie wybranego rodzaju sortowania
     */
    handleSortAction: function() {
        var urlParams = APP.getUrlParams();

        if("undefined" === typeof urlParams.sort) {
            return;
        }

        switch (urlParams.sort) {
            case 'byPages':
                APP.sortByPages();
                break;
            case 'byDate':
                APP.sortByDate();
                break;
            case 'byAuthor':
                APP.sortByFullName();
                //APP.sortBySurname();
                break;
        }
    },

    /**
     * Przekazanie danych z adresu do pól formularza
     */
    setParamsToForm: function() {
        var urlParams = APP.getUrlParams();

        document.getElementById('pages').value = urlParams.pages || '';

        var sortValue = urlParams.sort || '';

        var radio = document.getElementsByName('sort');

        for(var i = 0; i < radio.length; i++) {
            if("undefined" === typeof radio[i]) {
                continue;
            }

            radio[i].checked = false;

            if(radio[i].value == sortValue) {
                radio[i].checked = true;
                break;
            }
        }

        document.searchForm.elements.sort = sortValue;
    },

    /**
     * Akcje po dokonaniu zmiany w formularzu
     */
    searchFormAction: function() {
        var form = document.getElementById('searchForm');
        form.addEventListener('change', function() {
            var serializedForm = APP.serializeForm(form);
            history.pushState('', '', '?' + serializedForm);
            APP.restoreData();
            APP.setPagesLimit();
            APP.handleSortAction();
            APP.setBooks();
            APP.popupShowAction();
        });
    },

    /**
     *  Serializacja pól formularza w celu przekazania ich do adresu
     * @param {Element} form - Element formularza
     * @returns {string}
     */
    serializeForm: function(form) {
        var formElements = form.elements;
        var serialized = [];
        var len = formElements.length;
        var str = '';

        for(var i = 0; i < len; i++) {
            var formElement = formElements[i];
            var type = formElement.type;
            var name = formElement.name;
            var value = formElement.value;

            switch(type) {
                case 'text':
                case 'textarea':
                case 'number':
                    if(true === Boolean(value)) {
                        str = name + '=' + value;
                        serialized.push(str);
                    }
                    break;
                case 'checkbox':
                case 'radio':
                    if(true === formElement.checked) {
                        str = name + '=' + value;
                        serialized.push(str);
                    }
                    break;
                default:
                    break;
            }
        }
        return serialized.join('&');
    },

    /**
     * Wyczyszczenie wszystkich filtrów i kryteriów
     */
    clearFormAction: function() {

        var clear = function(e) {
            e.preventDefault();
            history.pushState('', '', '?');
            APP.restoreData();
            APP.setParamsToForm();
            APP.setBooks();
            APP.popupShowAction();
        };

        document.getElementById('searchFormClearButton').addEventListener('click', clear);

        document.addEventListener('keydown', function (e) {
            if(true === e.altKey && e.keyCode == 82) {
                clear(e);
            }
        });
    },

    /**
     * Utworzenie i wyświetlenie popup'a
     */
    popupShowAction: function() {
        var boxes = document.getElementsByClassName('js-box__image-holder');
        for(var i = 0; i < boxes.length; i++) {
            boxes[i].onclick = function(e) {
                e.preventDefault();

                var bigImageUrl = this.getAttribute('href');
                var popupImage = document.getElementsByClassName('js-popup__image');

                if("undefined" === typeof popupImage[0]) {
                    return;
                }

                var image = APP.createHtmlElement('img', 'popup__image', {'src': bigImageUrl});
                popupImage[0].innerHTML = "";
                popupImage[0].appendChild(image);

                var popup = document.getElementsByClassName('js-popup');

                if("undefined" === typeof popup[0]) {
                    return;
                }

                popup[0].className += " active";
            }
        }
    },

    /**
     * Ukrycie popup'a
     */
    hidePopupAction: function() {
        var popup = document.getElementsByClassName('js-popup');

        if("undefined" === typeof popup[0]) {
            return;
        }

        var closePopupButton = document.getElementsByClassName('js-popup__close');

        if("undefined" === typeof closePopupButton[0]) {
            return;
        }

        closePopupButton[0].addEventListener('click', function(e) {
            e.preventDefault();
            popup[0].className += "popup js-popup";
        });
    }
};

document.addEventListener("DOMContentLoaded", APP.start);